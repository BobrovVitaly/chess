export interface Cell {
  startX: string;
  startY: string;
  color: string;
  height: '100';
  width: '100';
}
