import { Injectable } from '@angular/core';
import {Cell} from '../models/interfaces';

@Injectable({
  providedIn: 'root'
})
export class ChessBoardService {

  private readonly cells: Cell[] = [];
  constructor() {
     this.setCellsValue();
  }

  private setCellsValue(): void {
    for (let i = 1; i <= 8; i++) {
      for (let j = 1; j <= 8; j++) {
        const cell: Cell = {
          startX: ((j - 1) * 100).toString(),
          startY: ((i - 1) * 100).toString(),
          height: '100',
          width: '100',
          color: (i + j) % 2 ? 'white' : 'black'
        };
        this.cells.push(cell);
      }
    }
  }

  public getCells(): Cell[] {
    return this.cells;
  }
}
