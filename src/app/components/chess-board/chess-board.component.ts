import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ChessBoardService} from '../../services/chess-board.service';
import {Cell} from '../../models/interfaces';

@Component({
  selector: 'app-chess-board',
  templateUrl: './chess-board.component.html',
  styleUrls: ['./chess-board.component.scss']
})
export class ChessBoardComponent implements OnInit {

  @ViewChild('board', {static: true}) board: ElementRef<Element>;

  private cells: Cell[];

  constructor(
    private chessBoardService: ChessBoardService,
  ) {
  }

  ngOnInit(): void {
    // this.cells = this.chessBoardService.getCells();
    // const cellGroupElements = document.createElement('g');
    // cellGroupElements.id = 'cell';
    // this.board.nativeElement.appendChild(cellGroupElements);
    // const rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
    // rect.width.baseVal.valueAsString = '100';
    // rect.height.baseVal.valueAsString = '100';
    // this.cells.forEach(c => {
    //   const useRect = document.createElementNS('http://www.w3.org/2000/svg', 'use');
    //   useRect.setAttribute('xlink:href', '#cell');
    //   useRect.x.baseVal.valueAsString = c.startX;
    //   useRect.y.baseVal.valueAsString = c.startY;
    //   useRect.width.baseVal.valueAsString = c.width;
    //   useRect.height.baseVal.valueAsString = c.height;
    //   useRect.style.fill = c.color;
    //   this.board.nativeElement.appendChild(useRect);
    // });
    // console.dir(this.cells);
    // console.dir(this.board);
  }

}
